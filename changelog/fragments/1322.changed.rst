The following MDA settings names have been modified:
- ``BaseMDA``: ``linear_solver_options`` is now ``linear_solver_settings``,
- ``MDANewtonRaphson``: ``newton_linear_solver_options`` is now ``newton_linear_solver_settings``,
- ``MDAChain``: ``inner_mda_options`` is now ``inner_mda_settings``,
                ``mdachain_parallel_options`` is now ``mdachain_parallel_settings``.

The following ``BaseMDA`` attributes names have been modified:
- ``BaseMDA.linear_solver`` is now accessed via ``BaseMDA.settings.linear_solver``,
- ``BaseMDA.linear_solver_options`` is now accessed via ``BaseMDA.settings.linear_solver_settings``,
- ``BaseMDA.linear_solver_tolerance`` is now accessed via ``BaseMDA.settings.linear_solver_tolerance``,
- ``BaseMDA.max_mda_iter`` is now accessed via ``BaseMDA.settings.max_mda_iter``,
- ``BaseMDA.tolerance`` is now accessed via ``BaseMDA.settings.tolerance``,
- ``BaseMDA.use_lu_fact`` is now accessed via ``BaseMDA.settings.use_lu_fact``,
- ``BaseMDA.warm_start`` is now accessed via ``BaseMDA.settings.warm_start``.

The inner MDA settings of ``MDAChain`` can no longer be passed using ``**inner_mda_options``, and must now be passed either as dictionnary or an instance of ``MDAChain_Settings``.

The signature of ``MDAGSNewton`` has been modified. Settings for the ``MDAGaussSeidel`` and the ``MDANewtonRaphson`` are now respectively passed via the ``gauss_seidel_settings`` and the ``newton_settings`` arguments, which can be either key/value pairs or the appropriate Pydantic settings model.

The MDA settings for the ``IDF`` formulation are now passed via the ``mda_chain_settings_for_start_at_equilibrium`` argument which can be either key/value pairs or an ``MDAChain_Settings`` instance.
The MDA settings for the ``MDF`` and ``BiLevel`` formulations are now passed via the ``main_mda_settings`` argument which can be either key/value pairs or an appropriate Pydantic settings model.
